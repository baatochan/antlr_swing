tree grammar TExpr3;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;

	output=template;
	superClass = TreeParserTmpl;
}

@header {
	package tb.antlr.kompilator;
}

@members {
	Integer ifIter = 0;
}


prog
	: (e+=expr | d+=decl)* -> printASM(name={$e},deklaracje={$d})
	;

decl
	: ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dec(name={$ID.text})
	;
	catch [RuntimeException ex] {errorID(ex,$i1);}

expr
	: ^(PLUS  e1=expr     e2=expr)                                         -> mathOp(op={"ADD"}, p1={$e1.st}, p2={$e2.st})
	| ^(MINUS e1=expr     e2=expr)                                         -> mathOp(op={"SUB"}, p1={$e1.st}, p2={$e2.st})
	| ^(MUL   e1=expr     e2=expr)                                         -> mathOp(op={"MUL"}, p1={$e1.st}, p2={$e2.st})
	| ^(DIV   e1=expr     e2=expr)                                         -> mathOp(op={"DIV"}, p1={$e1.st}, p2={$e2.st})
	| ^(PODST i1=ID       e2=expr)          {globals.getSymbol($ID.text);} -> set(name={$ID.text}, value={$e2.st})
	| ^(IF    be=boolExpr e1=expr e2=expr?)                                -> if(be={$be.st}, e1={$e1.st}, e2={$e2.st}, i={ifIter.toString()})
	| INT                                                                  -> int(i={$INT.text})
	| ID                                    {globals.getSymbol($ID.text);} -> get(name={$ID.text})
	;
	catch [RuntimeException ex] {errorID(ex,$i1);}

boolExpr
	: ^(EQUAL     e1=expr e2=expr) {ifIter++;} -> comp(jumpType={"JNE"}, e1={$e1.st}, e2={$e2.st}, i={ifIter.toString()})
	| ^(NOT_EQUAL e1=expr e2=expr) {ifIter++;} -> comp(jumpType={"JE"},  e1={$e1.st}, e2={$e2.st}, i={ifIter.toString()})
	| ^(GREATER   e1=expr e2=expr) {ifIter++;} -> comp(jumpType={"JNG"}, e1={$e1.st}, e2={$e2.st}, i={ifIter.toString()})
	| ^(LESS      e1=expr e2=expr) {ifIter++;} -> comp(jumpType={"JNL"}, e1={$e1.st}, e2={$e2.st}, i={ifIter.toString()})
	;
