grammar Expr;

options {
	output=AST;
	ASTLabelType=CommonTree;
}

@header {
	package tb.antlr;
}

@lexer::header {
	package tb.antlr;
}


prog
	: (stat )+ EOF!;

stat
	: expr NL -> expr

	| VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
	| ID PODST expr NL -> ^(PODST ID expr)

	| IF boolExpr THEN stat (ELSE stat)? -> ^(IF boolExpr stat (stat)?)

	| NL ->
	;

boolExpr
	: expr
	( EQUAL^ expr
	| NOT_EQUAL^ expr
	| GREATER^ expr
	| LESS^ expr
	)
	;

expr
	: multExpr
	( PLUS^ multExpr
	| MINUS^ multExpr
	)*
	;

multExpr
	: atom
	( MUL^ atom
	| DIV^ atom
	| MOD^ atom
	)*
	;

atom
	: INT
	| ID
	| LP! expr RP!
	;


IF : 'if';

THEN : 'then';

ELSE : 'else';

VAR :'var';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : ';';

WS : (' ' | '\t')+ {$channel = HIDDEN;};

LP : '(';

RP : ')';

EQUAL : '==';

NOT_EQUAL : '!=';

GREATER : '>';

LESS : '<';

PODST : '=';

PLUS : '+' ;

MINUS : '-';

MUL : '*';

DIV : '/';

MOD : '%';
